import { from } from "rxjs";
import { multiply } from './multiply';

const observable = from([1, 2, 3, 4, 5]);

const subscriber = {
  next: elm => {
    console.log(elm);
  },
  complete: comp => {
    console.log(comp);
  },
  error: err => {
    console.log(err);
  }
};

observable.pipe(multiply(3)).subscribe(subscriber);
