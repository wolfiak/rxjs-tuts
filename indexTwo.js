import {from, Subscriber } from 'rxjs';

const observable = from([1,2,3,4,5]);

const subscriber ={
    next: elm => {
        console.log(elm);
    },
    complete: comp => {
        console.log(comp);
    },
    error: err => {
        console.log(err);
    }
};

class DoubleSubscriber extends Subscriber{
    _next(value){
        this.destination.next(value * 2);
    }
}

observable.subscribe(new DoubleSubscriber(subscriber));
