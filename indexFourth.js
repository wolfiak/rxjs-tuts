import {from, Subscriber, Observable } from 'rxjs';

const observable = from([1,2,3,4,5]);

const subscriber ={
    next: elm => {
        console.log(elm);
    },
    complete: comp => {
        console.log(comp);
    },
    error: err => {
        console.log(err);
    }
};

class DoubleSubscriber extends Subscriber{
    _next(value){
        this.destination.next(value * 2);
    }
}

// const double = source => {
//     const o = new Observable();
//     o.source = source
//     o.operator = {
//         call(sub, source){
//             source.subscribe(new DoubleSubscriber(sub))
//         }
//     }
//     return o;
// }

const double = source => 
    source.lift({
        call(sub,source){
            source.subscribe(new DoubleSubscriber(sub));
        }
    })


observable
    .pipe(double).subscribe(subscriber);