import { from, Subscriber, Observable } from "rxjs";

const observable = from([1, 2, 3, 4, 5]);

const subscriber = {
  next: elm => {
    console.log(elm);
  },
  complete: comp => {
    console.log(comp);
  },
  error: err => {
    console.log(err);
  }
};

class MultiplySubscriber extends Subscriber {
  constructor(subscriber, number) {
    super(subscriber);
    this.number = number;
  }

  _next(value) {
    this.destination.next(value * this.number);
  }
}

class MapSubscriber extends Subscriber{
    constructor(subscriber, fun){
        super(subscriber);
        this.fun = fun;
    }
    _next(value) {
        this.destination.next(this.fun(value));
    }
}

const multiply = number => source =>
  source.lift({
    call(sub, source) {
      source.subscribe(new MultiplySubscriber(sub, number));
    }
  });

const map = fn => source =>
  source.lift({
    call(sub, source) {
        source.subscribe(new MapSubscriber(sub, fn));
    }
  });

observable
  .pipe(
    multiply(3),
    map(elm => elm + 4)
  )
  .subscribe(subscriber);
