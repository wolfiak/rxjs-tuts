import {from, Subscriber, Observable } from 'rxjs';

const observable = from([1,2,3,4,5]);

const subscriber ={
    next: elm => {
        console.log(elm);
    },
    complete: comp => {
        console.log(comp);
    },
    error: err => {
        console.log(err);
    }
};

class DoubleSubscriber extends Subscriber{
    _next(value){
        this.destination.next(value * 2);
    }
}

// const o = new Observable();
// o.source = observable;
// o.operator = {
//     call(sub, source){
//         source.subscribe(new DoubleSubscriber(sub))
//     }
// }

// o.subscribe(subscriber);

observable
    .pipe(
        source => {
            const o = new Observable();
            o.source = source
            o.operator = {
                call(sub, source){
                    source.subscribe(new DoubleSubscriber(sub))
                }
            }
            return o;
        }
    ).subscribe(subscriber);